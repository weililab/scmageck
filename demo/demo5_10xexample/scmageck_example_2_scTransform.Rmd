---
title: "Using scMAGeCK for 10x example datasets"
output: html_notebook
---

Checking requirements.

**Note**: If you are using BioConductor scMAGeCK package, they are probably not up-to-date. You can directly install the latest code in GitHub page. See [scMAGeCK manual](https://bitbucket.org/weililab/scmageck) for more details.

```{r}
require(Seurat)
require(scMAGeCK)
require(hdf5r)
```

# Identify and load necessary files

Load read counts that will contain two matrix, one from gene expression matrix, the other from cell identity matrix (i.e., which guide is detected in which cell).

```{r}
feat_bc_mat=Read10X_h5('data/SC3_v3_NextGem_DI_CellPlex_CRISPR_A549_30K_A549_Small_Pool_v2_No_Treatment_count_sample_feature_bc_matrix.h5')


```

Load feature table that includes the design of your screening library.

```{r}
feat_ref=read.csv('data/SC3_v3_NextGem_DI_CellPlex_CRISPR_A549_30K_A549_Small_Pool_v2_No_Treatment_count_feature_reference.csv',row.names = 1)

```

# Preprocessing: generate BARCODE information and Seurat object

## Create Seurate object

First, we create a Seurat object from count matrix

```{r}
feat_c=CreateSeuratObject(counts = feat_bc_mat$`Gene Expression`,project='10xCRISPR')


```

## Generate barcode information

Then, we will need to generate a cell identity frame (or file).

```{r}

#dgc_mat=as(feat_bc_mat$`CRISPR Guide Capture`,'dgTMatrix')
#cell_names=colnames(dgc_mat)
#guide_names=rownames(dgc_mat)

#bc_frame=data.frame(cell=cell_names[dgc_mat@j+1],
#                    barcode=guide_names[dgc_mat@i+1],
#                    read_count=dgc_mat@x,
#                    umi_count=dgc_mat@x)

bc_frame <- guidematrix_to_triplet(feat_bc_mat$`CRISPR Guide Capture`,feat_c)

bc_frame[,'gene'] <- feat_ref[bc_frame[,'barcode'],'target_gene_name']

bc_frame[,'sgrna'] <- feat_ref[bc_frame[,'barcode'],'sequence']

head(bc_frame)
```

Note that there will be a large number of cells with more than one guides detected. This may cause some issues for RRA since RRA only uses cells that are targeted by only one guide. 

We only keep guides that have greater than 2 reads. 

```{r}
bc_frame=bc_frame[bc_frame$read_count>2,]
```

Save to file (can be loaded later)

```{r}
write.table(bc_frame,file='BARCODE_10x.txt',row.names = F,sep='\t',quote=F)
BARCODE='BARCODE_10x.txt'
```




## Check the distribution of barcodes

```{r}
featurePlot(RDS = feat_c, BARCODE=BARCODE, TYPE = "Dis")
```


## Preprocess the guide count matrix

Note that this step only processes guide count matrix. Expression matrix will be processed later.

```{r}
feat_c=pre_processRDS(BARCODE = BARCODE, RDS = feat_c)




```

# Preprocessing: using Seurat

The pre-processing comes from [Seurat](https://satijalab.org/seurat/index.html) R package. Here we only demonstrates some basic preprocessing steps.


Pre-processing

```{r}
feat_c[["percent.mt"]] <- PercentageFeatureSet(feat_c, pattern = "^MT-")
VlnPlot(feat_c, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)
```

```{r}
plot1 <- FeatureScatter(feat_c, feature1 = "nCount_RNA", feature2 = "percent.mt")
plot2 <- FeatureScatter(feat_c, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
plot1 + plot2
```


## Normalization and scaling using SCTransform 

```{r, echo=F, results='hide'}

feat_c=SCTransform(feat_c,verbose = F)
```


## Perform linear dimensional reduction


```{r}
feat_c <- RunPCA(feat_c, features = VariableFeatures(object = feat_c))
print(feat_c[["pca"]], dims = 1:5, nfeatures = 5)
```

```{r}
VizDimLoadings(feat_c, dims = 1:2, reduction = "pca")
DimPlot(feat_c, reduction = "pca")
DimHeatmap(feat_c, dims = 1, cells = 500, balanced = TRUE)
```


## Cluster the cells, and Run non-linear dimensional reduction (UMAP/tSNE)


```{r}
feat_c <- FindNeighbors(feat_c, dims = 1:10)
feat_c <- FindClusters(feat_c, resolution = 0.5)
head(Idents(feat_c), 5)


```


UMAP visualization

```{r}
feat_c <- RunUMAP(feat_c, dims = 1:10)

DimPlot(feat_c, reduction = "umap")
```


# scMAGeCK example 1: test the association with a known marker using scMAGeCK-RRA

Now, we can run scMAGeCK RRA. As an example, let's see how EZH2 knockout suppresses EZH2 expression:

```{r}


target_gene='EZH2'

label='RRA_result'

rra_result<-scmageck_rra(BARCODE=bc_frame,RDS=feat_c,GENE=target_gene,LABEL=label,KEEPTMP = FALSE,NEGCTRL='Non-Targeting',SLOT = 'data')

```

Now, check the results, by looking at genes whose perturbation reduces target gene expression:

```{r}
rra_result[rra_result$FDR.low<0.1,]
```

```{r}
selectPlot(RRA_re1 = rra_result, RRA_re2 = NULL, CUTOFF = 0.05, QUALITY = 10, TYPE = "rra")

```

Before feature plot, assign each single cell with its target gene

```{r}
feat_c=assign_cell_identity(bc_frame,feat_c,ASSIGNMETHOD = 'largest')
table(feat_c@meta.data$gene)
```

As an example, let's just look at cells with EZH2-targeting guides on EZH2 expression.

```{r}
featurePlot(RDS = feat_c, GENE = "EZH2", sgRNA = "EZH2", TYPE = "Vln")

```


## visualize scMAGeCK results

First, look at gene expressions

```{r}
DefaultAssay(feat_c)<-"RNA"
FeaturePlot(feat_c,features = 'EZH2')
```

You can also look at guide expressions

```{r}
DefaultAssay(feat_c)<-"sgRNA"
FeaturePlot(feat_c,features = 'EZH2')
```

Recover default assay

```{r}
DefaultAssay(feat_c)<-"SCT"
```

## scMAGeCK example 2: LR module for large-scale association test.

Let's test scMAGeCK LR module. This step may take some time.


```{r}


label='LR_result'

lr_result<-scmageck_lr(BARCODE=BARCODE,RDS=feat_c,LABEL=label,NEGCTRL='Non-Targeting',PERMUTATION = 100,GENE_FRAC = 0.4)

lr_score <- lr_result[[1]]
lr_score_pval <- lr_result[[2]]
```


Check the results. Note that if a target gene is reduced upon perturbation, you should expect to see a negative LR score.

```{r}
lr_score[1:5,1:5]
```

```{r}
lr_score_pval[1:5,1:5]
```

```{r}
lr_score[1:10,colnames(lr_score)%in%rownames(lr_score)[1:10]]
```

