#FROM davidliwei/seurat-mageck
FROM satijalab/seurat

RUN apt-get update 
RUN apt-get install -y libgeos-dev
# RUN apt-get install -y pandoc 

# COPY . /app

# WORKDIR /app

# RUN R --vanilla -e 'library(reticulate);use_python("/usr/local/bin/python");py_config()'
# RUN R --vanilla -e 'install.packages(c("rmarkdown","knitr"), repos="https://cloud.r-project.org")'
# RUN R CMD build bioconductor ; ls -la; R CMD INSTALL scMAGeCK*.gz

RUN R --vanilla -e 'install.packages(c("devtools"), repos="https://cloud.r-project.org")'

RUN R --vanilla -e 'library(devtools);install_github("weililab/scMAGeCK")'

ENTRYPOINT echo "Welcome to scMAGeCK Docker" &  /bin/bash 

